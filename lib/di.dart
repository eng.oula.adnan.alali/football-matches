import 'package:dio/dio.dart';
import 'package:football_app/core/api/dio_helper.dart';
import 'package:football_app/core/api/interceptors.dart';
import 'package:football_app/features/fixture/fixture_di.dart';
import 'package:football_app/features/soccer/soccer_di.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;

void initApp() {
  initCore();
  initSoccer();
  initFixture();
}

void initCore() {
  sl.registerLazySingleton<Dio>(() => Dio());
  sl.registerLazySingleton<AppInterceptors>(() => AppInterceptors());

  sl.registerLazySingleton<LogInterceptor>(
    () => LogInterceptor(
      error: true,
      request: true,
      requestBody: true,
      requestHeader: true,
      responseBody: true,
      responseHeader: true,
    ),
  );
  sl.registerLazySingleton<DioHelper>(() => DioHelper(dio: sl<Dio>()));
}

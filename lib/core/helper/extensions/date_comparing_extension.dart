extension DateOnlyCompare on DateTime {
  bool isSameDate(DateTime other) {
    return this.year == other.year && this.month == other.month
        && this.day == other.day;
  }
}

extension GetDateFormat on DateTime {
 String getDateWithOutTime() {
 return this.toString().substring(0, this.toString().trim().indexOf(' '));
 }
}
import 'package:intl/intl.dart' as intl;

extension CheckLang on String {
  bool checkLang() {
    if (this != null && trim().isNotEmpty) {
      return intl.Bidi.detectRtlDirectionality(this[0]);
    } else {
      return false;
    }
  }
}

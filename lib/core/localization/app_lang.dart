import 'package:rxdart/rxdart.dart';

var appLanguage = BehaviorSubject<int>();
AppLanguages globalLanguage = AppLanguages.EN;

changeLanguage(int lang) {
  appLanguage.sink.add(lang);
}

enum AppLanguages { AR, EN }

mapIntToLang(int index){
  return index == 0 ?  AppLanguages.AR : AppLanguages.EN;
}
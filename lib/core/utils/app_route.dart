import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:football_app/core/domain/entities/soccer_fixture.dart';
import 'package:football_app/core/ui/splash_screen.dart';
import 'package:football_app/di.dart';
import 'package:football_app/features/fixture/domain/use_cases/lineups_usecase.dart';
import 'package:football_app/features/fixture/presentation/cubit/fixture_cubit.dart';
import 'package:football_app/features/fixture/presentation/screens/fixture_screen.dart';
import 'package:football_app/features/soccer/presentation/cubit/soccer_cubit.dart';
import 'package:football_app/features/soccer/presentation/screens/soccer_layout.dart';

class Routes {
  static const String splash = "splash";
  static const String soccerLayout = "soccerLayout";
  static const String fixture = "fixture";
}

class AppRouter {
  static Route routesGenerator(RouteSettings settings) {
    switch (settings.name) {
      case Routes.splash:
        return MaterialPageRoute(
          builder: (context) => SplashScreen(),
        );

      case Routes.soccerLayout:
        return MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => sl<SoccerCubit>(),
            child: const SoccerLayout(),
          ),
        );

      case Routes.fixture:
        return MaterialPageRoute(
          builder: (context) {
            SoccerFixture soccerFixture = settings.arguments as SoccerFixture;
            return BlocProvider(
              create: (context) => FixtureCubit(
                lineupsUseCase: sl<LineupsUseCase>(),
              )..getLineups(soccerFixture.fixture.id.toString()),
              child: FixtureScreen(soccerFixture: soccerFixture),
            );
          },
        );
    }
    return MaterialPageRoute(
      builder: (context) => Center(
        child: Text("Not Found"),
      ),
    );
  }
}

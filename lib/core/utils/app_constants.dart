import '../../features/soccer/domain/entities/league_of_fixture.dart';

class AppConstants {
  static const String baseUrl = "https://v3.football.api-sports.io/";
  static const int timeOutDuration = 90;
  static const int maxNameLength = 3;

  static const String API_KEY = "3c2a48b41864845b14fc781a656c0042";
/*"72aa3d9c7800379ca09e3957e1ba7fb2"*/
  static const int TIMEOUT = 20000;

//apis
  static const FIXTURES = "fixtures";
  static const LEAGUES = "leagues";
  static const LINEUPS = "lineups";

  static var availableLeagues = [
    1,
    2,
    3,
    4,
    5,
    6,
    12,
    15,
    20,
    22,
    25,
    40,
    50,
    60,
    61,
    65,
    70,
    75,
    78,
    80,
    90,
    100,
    110,
    120,
    125,
    135,
    140,
  ];
  static Map<int, LeagueOfFixture> leaguesFixtures = {};
}

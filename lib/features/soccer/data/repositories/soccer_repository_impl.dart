import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:football_app/core/error/error_handler.dart';
import 'package:football_app/features/soccer/domain/repositories/soccer_repository.dart';

import '../../../../core/domain/entities/league.dart';
import '../../../../core/domain/entities/soccer_fixture.dart';
import '../../../../core/domain/mappers/mappers.dart';
import '../datasources/soccer_data_source.dart';

class SoccerRepositoryImpl implements SoccerRepository {
  final SoccerDataSource soccerDataSource;

  SoccerRepositoryImpl({
    required this.soccerDataSource,
  });

  @override
  Future<Either<Failure, List<SoccerFixture>>> getDayFixtures(
      {required String date}) async {
    try {
      final result = await soccerDataSource.getDayFixtures(date: date);
      List<SoccerFixture> fixtures =
          result.map((fixture) => fixture.toDomain()).toList();
      return Right(fixtures);
    } on DioError catch (error) {
      return Left(ErrorHandler.handle(error).failure);
    }
  }

  @override
  Future<Either<Failure, List<League>>> getLeagues() async {
    try {
      final result = await soccerDataSource.getLeagues();
      List<League> leagues = result.map((league) => league.toDomain()).toList();
      return Right(leagues);
    } on DioError catch (error) {
      return Left(ErrorHandler.handle(error).failure);
    }
  }
}

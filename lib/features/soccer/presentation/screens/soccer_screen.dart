import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/domain/entities/soccer_fixture.dart';
import '../../../../core/error/response_status.dart';
import '../../../../core/utils/app_colors.dart';
import '../../../../core/widgets/center_indicator.dart';
import '../cubit/soccer_cubit.dart';
import '../cubit/soccer_state.dart';
import '../widgets/block_dialog.dart';
import 'fixtures_page.dart';

class SoccerScreen extends StatefulWidget {
  const SoccerScreen({Key? key}) : super(key: key);

  @override
  State<SoccerScreen> createState() => _SoccerScreenState();
}

class _SoccerScreenState extends State<SoccerScreen> {
  List<SoccerFixture> fixtures = [];

  @override
  void initState() {
    super.initState();
    getLists();
  }

  getLists() async {
    SoccerCubit cubit = context.read<SoccerCubit>();
    if (cubit.filteredLeagues.isEmpty) {
      await cubit.getLeagues();
    }
    if (cubit.filteredLeagues.isNotEmpty && fixtures.isEmpty) {
      fixtures = await cubit.getFixtures();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SoccerCubit, SoccerStates>(
      listener: (context, state) {
        if (state is SoccerLeaguesSuccess && state.leagues.isEmpty) {
          buildBlockAlert(
              context: context,
              message: 'You have reached the request limit for the day');
        }
        if (state is SoccerFixturesFailure &&
            state.message ==
                DataSource.networkConnectError.getFailure().message) {
          buildBlockAlert(context: context, message: state.message);
        }

        if (state is SoccerLeaguesFailure &&
            state.message ==
                DataSource.networkConnectError.getFailure().message) {
          buildBlockAlert(context: context, message: state.message);
        }
      },
      builder: (context, state) {
        SoccerCubit cubit = context.read<SoccerCubit>();
        return state is SoccerFixturesLoading || state is SoccerLeaguesLoading
            ? centerIndicator()
            : RefreshIndicator(
                onRefresh: () async {
                  await cubit.getFixtures();
                },
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 20,
                      top: 5,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FixturesPage(fixtures: fixtures),
                      ],
                    ),
                  ),
                ),
              );
      },
    );
  }
}

Gradient getGradientColor(SoccerFixture fixture) {
  Gradient color = AppColors.blueGradient;
  if (fixture.goals.away != fixture.goals.home) {
    color = AppColors.redGradient;
  }
  return color;
}

import 'package:flutter/material.dart';
import 'package:football_app/core/utils/app_route.dart';

import '../../../../core/domain/entities/soccer_fixture.dart';
import '../../../../core/media_query.dart';
import '../../../../core/utils/app_colors.dart';
import '../widgets/fixture_card.dart';

class FixturesPage extends StatelessWidget {
  final List<SoccerFixture> fixtures;

  const FixturesPage({Key? key, required this.fixtures}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return fixtures.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.only(right: 20),
            child: Column(
              children: [
                Row(
                  children: const [
                    Icon(Icons.calendar_today_sharp, color: AppColors.darkBlue),
                    SizedBox(width: 5),
                    Expanded(
                      child: Text(
                        "27-9-2022",
                        style: TextStyle(
                            color: AppColors.darkBlue,
                            fontWeight: FontWeight.bold,
                            fontSize: 15),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 15),
                ...List.generate(
                  fixtures.length,
                  (index) {
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).pushNamed(Routes.fixture,
                            arguments: fixtures[index]);
                      },
                      child: FixtureCard(
                        soccerFixture: fixtures[index],
                      ),
                    );
                  },
                ),
              ],
            ),
          )
        : SizedBox(
            height: context.height / 2,
            child: SingleChildScrollView(
              child: Text(
                "No Fixtures",
                style: TextStyle(
                  fontSize: 20,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          );
  }
}

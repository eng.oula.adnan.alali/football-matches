import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../core/domain/entities/soccer_fixture.dart';
import '../../../../core/utils/app_colors.dart';

///todo
class FixtureCard extends StatelessWidget {
  final SoccerFixture soccerFixture;

  const FixtureCard({Key? key, required this.soccerFixture}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DateFormat dateFormat = DateFormat('yyyy-MM-dd \n hh:mm');
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Padding(
        padding: const EdgeInsetsDirectional.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            buildTeamInfo(
              context,
              name: soccerFixture.teams.home.name,
              logo: soccerFixture.teams.home.logo,
            ),
            Expanded(
              child: (soccerFixture.goals.home != null &&
                      soccerFixture.goals.away != null)
                  ? Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              soccerFixture.goals.home.toString(),
                              style: TextStyle(
                                  color: AppColors.darkGreen,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                            Text(
                              " - ",
                              style: TextStyle(
                                  color: AppColors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                            Text(
                              soccerFixture.goals.away.toString(),
                              style: TextStyle(
                                  color: AppColors.deepOrange,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ],
                        ),
                        const SizedBox(height: 10),
                        Text(
                          dateFormat.format(
                              DateTime.parse(soccerFixture.fixture.date)),
                          style: TextStyle(
                              color: AppColors.blueGrey, fontSize: 12),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    )
                  : Center(
                      child: Text(
                        " No result available for this match ",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: AppColors.grey,
                            fontWeight: FontWeight.bold,
                            fontSize: 13),
                      ),
                    ),
            ),
            buildTeamInfo(
              context,
              name: soccerFixture.teams.away.name,
              logo: soccerFixture.teams.away.logo,
            ),
          ],
        ),
      ),
    );
  }
}

Expanded buildTeamInfo(BuildContext context,
    {required String logo, required String name}) {
  return Expanded(
    child: Column(
      children: [
        Image(
          fit: BoxFit.cover,
          height: 45,
          width: 45,
          image: NetworkImage(logo),
        ),
        const SizedBox(height: 10),
        Text(
          name,
          textAlign: TextAlign.center,
          maxLines: 1,
          overflow: TextOverflow.fade,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 15,
          ),
        ),
      ],
    ),
  );
}

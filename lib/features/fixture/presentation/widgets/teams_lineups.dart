import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '../../../../core/utils/app_colors.dart';
import '../../domain/entities/lineups.dart';
import '../../domain/entities/player.dart';

class TeamsLineups extends StatelessWidget {
  final List<Lineup> lineups;

  const TeamsLineups({Key? key, required this.lineups}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> teamOnePlan = lineups[0].formation.split("-");
    for (int i = 0; i < teamOnePlan.length; i++) {
      if (kDebugMode) {
        print("teamOnePlan   -->   " + teamOnePlan[i]);
      }
    }
    Iterable<String> teamTwoPlan = lineups[1].formation.split("-").reversed;
    List<Player> teamOnePlayers = lineups[0].startXI;
    for (int i = 0; i < teamOnePlayers.length; i++) {
      if (kDebugMode) {
        print("teamOnePlayers   -->   " + teamOnePlayers[i].toString());
      }
    }
    Iterable<Player> teamTwoPlayers = lineups[1].startXI.reversed;

    int lineOneNumber = 0;
    int lineTwoNumber = -1;

    return Column(
      children: [
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  CircleAvatar(
                    radius: 15,
                    backgroundColor:
                        HexColor("#${lineups[0].team.colors.player.primary}"),
                    child: Text(
                      teamOnePlayers[0].number.toString(),
                      style: TextStyle(
                          color: HexColor(
                              "#${lineups[0].team.colors.player.number}")),
                    ),
                  ),
                  Text(
                    teamOnePlayers[0].name,
                    style: TextStyle(color: AppColors.white),
                  ),
                ],
              ),
              ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                physics: const BouncingScrollPhysics(),
                separatorBuilder: (context, index) =>
                    const SizedBox(height: 10),
                itemBuilder: (context, index) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ...List.generate(
                        int.parse(teamOnePlan[index]),
                        (_) {
                          lineOneNumber++;
                          List<String> playerName =
                              teamOnePlayers[lineOneNumber].name.split(" ");
                          return Expanded(
                            child: Column(
                              children: [
                                CircleAvatar(
                                  radius: 15,
                                  backgroundColor: HexColor(
                                      "#${lineups[0].team.colors.player.primary}"),
                                  child: Text(
                                    teamOnePlayers[lineOneNumber]
                                        .number
                                        .toString(),
                                    style: TextStyle(
                                      color: HexColor(
                                          "#${lineups[0].team.colors.player.number}"),
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 2),
                                Text(
                                  playerName[1],
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: AppColors.white,
                                    fontSize: 12,
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ],
                  );
                },
                itemCount: teamOnePlan.length,
              ),
            ],
          ),
        ),
        const SizedBox(height: 10),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                physics: const BouncingScrollPhysics(),
                separatorBuilder: (context, index) =>
                    const SizedBox(height: 10),
                itemBuilder: (context, index) => Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ...List.generate(
                      int.parse(teamTwoPlan.elementAt(index)),
                      (_) {
                        lineTwoNumber++;
                        List<String> playerName = teamTwoPlayers
                            .elementAt(lineTwoNumber)
                            .name
                            .split(" ");
                        return Expanded(
                          child: Column(
                            children: [
                              Text(
                                playerName.length == 2
                                    ? playerName[1]
                                    : playerName[0],
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: AppColors.white,
                                  fontSize: 12,
                                ),
                              ),
                              const SizedBox(height: 2),
                              CircleAvatar(
                                radius: 15,
                                backgroundColor: HexColor(
                                  "#${lineups[1].team.colors.player.primary}",
                                ),
                                child: Text(
                                  teamTwoPlayers
                                      .elementAt(lineTwoNumber)
                                      .number
                                      .toString(),
                                  style: TextStyle(
                                      color: HexColor(
                                          "#${lineups[1].team.colors.player.number}")),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ],
                ),
                itemCount: teamTwoPlan.length,
              ),
              Column(
                children: [
                  Text(
                    teamTwoPlayers.elementAt(10).name,
                    style: TextStyle(color: AppColors.white),
                  ),
                  CircleAvatar(
                    radius: 15,
                    backgroundColor:
                        HexColor("#${lineups[1].team.colors.player.primary}"),
                    child: Text(
                      teamTwoPlayers.elementAt(10).number.toString(),
                      style: TextStyle(
                          color: HexColor(
                              "#${lineups[1].team.colors.player.number}")),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

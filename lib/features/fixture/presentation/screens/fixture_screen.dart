import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/domain/entities/soccer_fixture.dart';
import '../../../../core/utils/app_colors.dart';
import '../cubit/fixture_cubit.dart';
import '../widgets/fixture_details.dart';
import '../widgets/lineups_view.dart';

///todo
class FixtureScreen extends StatelessWidget {
  final SoccerFixture soccerFixture;

  const FixtureScreen({Key? key, required this.soccerFixture})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    FixtureCubit cubit = context.read<FixtureCubit>();

    return BlocBuilder<FixtureCubit, FixtureState>(
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              "${soccerFixture.teams.home.name} VS ${soccerFixture.teams.away.name}",
            ),
            backgroundColor: AppColors.black,
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(
                Icons.keyboard_backspace,
                color: AppColors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: ListView(
            physics: const BouncingScrollPhysics(),
            children: [
              FixtureDetails(soccerFixture: soccerFixture),
              buildTitle(cubit),
              if (state is FixtureLineupsLoading)
                LinearProgressIndicator(
                  color: AppColors.darkGreen,
                  minHeight: 5.0,
                ),
              if (state is FixtureLineupsSuccess)
                LineupsView(lineups: state.lineups),
            ],
          ),
        );
      },
    );
  }

  Widget buildTitle(FixtureCubit cubit) => MaterialButton(
        onPressed: () async {
          await cubit.getLineups(soccerFixture.fixture.id.toString());
        },
        color: AppColors.grey,
        elevation: 6.0,
        padding: const EdgeInsets.all(16),
        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero),
        child: Text("Lineups",
            style: const TextStyle(
              color: AppColors.white,
              fontSize: 15,
              fontWeight: FontWeight.w500,
            )),
      );
}

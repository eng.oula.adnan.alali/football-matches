import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import '../../../../core/error/error_handler.dart';
import '../../domain/entities/lineups.dart';
import '../../domain/mappers/mappers.dart';
import '../../domain/repositories/fixture_repository.dart';
import '../data_sources/fixture_data_source.dart';

class FixtureRepositoryImpl implements FixtureRepository {
  final FixtureDataSource fixtureDataSource;

  FixtureRepositoryImpl({required this.fixtureDataSource});

  @override
  Future<Either<Failure, List<Lineup>>> getLineups(String fixtureId) async {
    try {
      final result = await fixtureDataSource.getLineups(fixtureId);
      List<Lineup> lineups = result.map((lineup) => lineup.toDomain()).toList();
      return Right(lineups);
    } on DioError catch (error) {
      return Left(ErrorHandler.handle(error).failure);
    }
  }
}
